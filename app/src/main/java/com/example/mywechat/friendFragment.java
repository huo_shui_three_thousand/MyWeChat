package com.example.mywechat;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class friendFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<String> list;
    private View view;
    private adapter adapter;
   // private  Context context;

    public friendFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tab05, container, false);
        initData();
        initview();
        return view;
    }

    private void initData(){
        list = new ArrayList<String>();
        for (int i=0;i<15;i++){
            list.add("这是第"+i+"个例子！");

        }
    }

    private void initview(){
        recyclerView =(RecyclerView)view.findViewById(R.id.recycleview);
        //context=getActivity();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        adapter = new adapter(getActivity(),list);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);

        //划线
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
    }

}
