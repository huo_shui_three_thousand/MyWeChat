package com.example.mywechat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactsViewHolder> {

    private List<ContactDataBean> contactsAccounts;
    private Context mContext;

    public ContactAdapter(Context context, List<ContactDataBean> contactsAccounts) {
        mContext=context;
        this.contactsAccounts = contactsAccounts;
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.contact_item, parent, false);
        return new ContactsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsViewHolder holder, int position) {

        holder.tv_Name2.setText(contactsAccounts.get(position).getName());
        holder.tv_Phone2.setText(contactsAccounts.get(position).getPhoneNumber());

    }


    @Override
    public int getItemCount() {
        return contactsAccounts.size();
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder {

        LinearLayout LLName,LLPhone;
        TextView tv_Name1, tv_Name2,tv_Phone1,tv_Phone2;
        public ContactsViewHolder(@NonNull View itemView) {
            super(itemView);

            LLName=itemView.findViewById(R.id.LLName);
            LLPhone=itemView.findViewById(R.id.LLPhone);
            tv_Name1=itemView.findViewById(R.id.tv_Name1);
            tv_Name2=itemView.findViewById(R.id.tv_Name2);
            tv_Phone1=itemView.findViewById(R.id.tv_Phone1);
            tv_Phone2=itemView.findViewById(R.id.tv_Phone2);

        }
    }
}
