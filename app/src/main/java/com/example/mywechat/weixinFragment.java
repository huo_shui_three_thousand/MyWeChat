package com.example.mywechat;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class weixinFragment extends Fragment {

    private static final String TAG = MainActivity.class.getSimpleName();

    private List<String> mList = new ArrayList<>();

    private List<GroupDataBean> mDataList = new ArrayList<>();

    private View view;

    public weixinFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.tab01, container, false);
        view=inflater.inflate(R.layout.tab07, container, false);
        initList();
        initData();
        initView();
        return view;

    }

    private void initList() {
        mList.add("武汉过早|热干面");
        mList.add("武汉过早|豆皮");
        mList.add("武汉过早|油饼包烧卖");
        mList.add("武汉过早|面窝");
        mList.add("武汉过早|糯米鸡");
        mList.add("江西|三杯鸡");
        mList.add("江西|石鱼炒蛋");
        mList.add("江西|庐山石鸡");
        mList.add("江西|金线吊葫芦");
        mList.add("江西|米粉蒸肉");
        mList.add("湖南|油粑粑");
        mList.add("湖南|长沙臭豆腐");
        mList.add("湖南|口味虾");
        mList.add("湖南|辣椒炒肉");
        mList.add("广东|肠粉");
        mList.add("广东|蜜汁叉烧");
        mList.add("广东|马蹄糕");
        mList.add("广东|虾饺");
        mList.add("广东|煲仔饭");
        mList.add("广东|双皮奶");
        mList.add("广东|炒河粉");
        mList.add("广东|艇仔粥");
        mList.add("四川|水煮肉片");
        mList.add("四川|夫妻肺片");
        mList.add("四川|毛血旺");
        mList.add("四川|龙抄手");
        mList.add("江苏|鸭血粉丝汤");
        mList.add("江苏|盐水鸭");
        mList.add("江苏|富春名点");
        mList.add("江苏|千层油糕");
    }

    private void initData() {
        for (int i = 0; i < mList.size(); i++) {
            GroupDataBean bean = new GroupDataBean();

            String s = mList.get(i);
            // area
            String area = s.substring(0, s.indexOf("|"));
            // team
            String team = s.substring(s.indexOf("|") + 1, s.length());

            bean.setArea(area);
            bean.setTeam(team);

            mDataList.add(bean);
        }

        Log.d(TAG, "initData: " + mDataList.size());
    }

    private void initView() {
        GroupAdapter adapter = new GroupAdapter(getActivity());

        RecyclerView rcvGroup = view.findViewById(R.id.rcv_group);

        rcvGroup.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvGroup.setHasFixedSize(true);
        rcvGroup.setAdapter(adapter);

        adapter.setGroupDataList(mDataList);
    }




}
